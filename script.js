// Теоретический вопрос
//
// Опишите своими словами, что такое метод обьекта
// Методы обьекта - это встроенные в объект функции, которые работают с переменными внутри объекта и передаными ему в качестве аргументов значениями.


// Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя,
//     соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
//     Вывести в консоль результат выполнения функции.
//
//     Необязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(),
//     которые позволят изменить данные свойства.




function createNewUser() {
    this.firstName = prompt('Enter your first name ','');
    while (this.firstName === ''){
        this.firstName = prompt('Please enter your first name again ','');
    }

    this.lastName = prompt('Enter your last name','');
    while (this.lastName === ''){
        this.lastName = prompt('Please enter your last name again ','');
    }

    this.getLogin = function(){
        let newLogin = this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
        return newLogin;
    }
}

let newUserObj = new createNewUser();
alert(`Your login is ${newUserObj.getLogin()}`);


